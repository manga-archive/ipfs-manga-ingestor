import fetch from 'node-fetch';
import {mkdir, writeFile} from 'fs/promises'
import {createHmac} from 'crypto'
import ipfsClient from 'ipfs-http-client'
import path from 'path'
const multihash = ipfsClient.multihash 

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

export default async function dlManga(manga_id, ipfs, options={}, tld='org') {
	let mangaInfo

	try{
		let res = await fetch(`https://mangadex.${tld}/api/manga/${manga_id}/`)
		if(!res.ok) throw 'shit fucked:\n' + res
		mangaInfo = await res.json()
	}
	catch (e) {
		console.error(e)
		return;
	}

	let chapters = mangaInfo.chapter
	if(!!options.lang_code) chapters = chapters.filter(ch => ch.lang_code === lang_code)

	for(const chapter_id in chapters){
		await dlChapter(chapter_id, ipfs)
		await wait(2000)
	}
}


export async function dlChapter(chapter_id, ipfs, tld="org", save=false, savePath){
	console.log('[ STARTING ADD ]:', chapter_id)
	let chapterInfo

	try{
		let res = await fetch(`https://mangadex.${tld}/api/chapter/${chapter_id}/`)
		chapterInfo = await res.json()
		if(!res.ok){
			throw chapterInfo.message
		}
	}
	catch (e) {
		if(chapterInfo.message)
			console.warn(chapterInfo.message)
		else console.warn(e)
		return
	}

	let {
		id, 
		manga_id,
		hash,
		chapter,
		title,
		volume,
		timestamp,  
		lang_code, 
		group_name,
		group_name_2,
		group_name_3
	} = await chapterInfo

	let groups = []
	if(group_name) {groups.push(group_name)}
	if(group_name_2) {groups.push(group_name_2)}
	if(group_name_3) {groups.push(group_name_3)}

	let saveInfo = {
		version: 0,
		source: 'mangadex',
		mangadex_info:{
			chapter_id: id,
			chapter_hash: hash,
			manga_id,
		},
		title,
		volume,
		chapter,
		timestamp,  
		lang_code, 
		groups,
		page_unchunked_multihashes: [],
		page_cids: []
	}

	let pages = chapterInfo.page_array
	let chapterHost = chapterInfo.server + chapterInfo.hash

	if(!pages) throw new Error('no pages')

	if(save) await mkdir(savePath, { recursive: true }); 

	let pagesExt = []
	let downloads = pages.map((page_id, index, array) => {
		return dlPage(chapterHost+'/'+page_id, page_id, save, savePath)
		.then((page)=>{
			pagesExt[index] = path.extname(page_id)
			let pageHash
			let pageMultihash
			if(page_id.includes('-')){
				let parsedName = page_id.split(/(?!\d-)(\w+)(?=\.\w+)/)
				
				// https://multiformats.io/multihash/#sha2-256-256-bits-aka-sha256
				// 0x12: sha2-256
				// 0x20: 32 bytes long
				pageHash = '1220'+ parsedName[1]
				
				pageMultihash = Buffer.from(pageHash, 'hex')
				multihash.validate(pageMultihash)

				saveInfo.page_unchunked_multihashes[index] = pageHash
			} else {

				let digest = createHmac('sha256', page).digest()
				pageMultihash = multihash.encode(digest, 'sha2-256')
				multihash.validate(pageMultihash)
				pageHash = pageMultihash.toString('hex')

				saveInfo.page_unchunked_multihashes[index] = pageHash
			}


			saveInfo.page_cids[index] = ipfs.add(page, {rawLeaves: true})
			return page
		})
		.catch((e) => console.log(e))
	});

	downloads = await Promise.all(downloads).catch(e => console.log(e))
	saveInfo.page_cids = await Promise.all(saveInfo.page_cids).catch(e => console.log(e))
	
	let totalSize = 0
	saveInfo.page_cids = saveInfo.page_cids.map( element => {
		totalSize += element.size
		return element.path
	})

	if(save) writeFile(savePath + '/info.json', JSON.stringify(saveInfo, null, 2))
	if(saveInfo.page_unchunked_multihashes.length !== chapterInfo.page_array.length) throw new Error('the amount of pages hashed and the ones avaliable do not match!')
	
	console.log('[ DOWNLOADED ]\n', {id:chapterInfo.id, pages:chapterInfo.page_array.length, totalSize, chapterHost})
	return save ? savePath : {info:saveInfo, downloads, pagesExt}
}

export async function dlPage(url, page_id, save=false, savePath){
	const response = await fetch(url);
	const buffer = await response.buffer();
	
	if (response.ok) {
		if(save) await writeFile(savePath + '/' +  parseInt(page_id.replace(/\D+/g, '')) + path.extname(page_id), buffer)
		return buffer
	}

	throw new Error(`unexpected response:\n\t ${response.statusText} url: ${url}`);
}

// async function dls (start, end){
// 	for (let index = start; index < end; index++) {
// 		await dlChapter(index)
// 		.then(() => {return wait(2000)})
// 		.catch((e) => console.log(e))
// 	}
// }