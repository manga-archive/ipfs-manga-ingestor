import {dlChapter} from './mdDL.js'
import IpfsClient from 'ipfs-http-client'
import OrbitDB from 'orbit-db'
import {readFile, opendir} from 'fs/promises'

let start = async () => {
	const ipfs = IpfsClient()

	const orbitdb = await OrbitDB.createInstance(ipfs)

	const db = await orbitdb.log('mangadex.uploads')
	await db.load()
	// await db.drop() //LOL dont use this 
	let addFolder = async (chapter, ipfsPath) => {
		let info = chapter.info
		// console.log(chapter.downloads)
		let infoBuf = Buffer.from(JSON.stringify(info, null, 2))
		// console.log(infoBuf)
		await ipfs.files.write(ipfsPath + `/info.json`, infoBuf , {parents: true, create: true, rawLeaves: true})
		chapter.downloads.map( async (page, index) => {
			// let bufEq = async (element) => page.equals(element)
			// console.log(chapter.downloads.indexOf(page))
			await ipfs.files.write(ipfsPath + `/${index}${chapter.pagesExt[index]}`, page, {parents: true, create: true, rawLeaves: true})
		})

		let folderStat = await ipfs.files.stat(ipfsPath)
		let cid = folderStat.cid
		return {cid, info}
	}

	let chId = 1000851


	addFolder(await dlChapter(chId, ipfs), `/manga/${chId}`).then( async stat => {
		let hash = await db.add(stat.cid.toString())
		// console.log(stat.info)
		console.log('[ ADDED ]  CID: ', stat.cid.toString(), hash)
		const all = db.iterator({ limit: -1 })
		.collect()
		.map((e) => e.payload.value)
		console.log(all)
	}).catch(e => console.error(e))
}


// orbitInit()


// let savePath = `${__dirname}/manga/${chapterInfo.id}`


let testprovs = async () => {
	const providers = ipfs.dht.findProvs('QmdPAhQRxrDKqkGPvQzBvjYe3kU8kiEEAd2J6ETEamKAD9')

	for await (const provider of providers) {
	console.log(provider.id.toString())
	const info = await ipfs.dht.findPeer(provider.id.toString())

	console.log(info.id)
	}

}

start()

const dropLocalOrbitDb = async (address) => {
	const ipfs = IpfsClient()

	const orbitdb = await OrbitDB.createInstance(ipfs)
	const db = await orbitdb.log('mangadex.uploads')
	await db.load()
	await db.drop()
	const all = db.iterator({ limit: -1 })
	.collect()
	.map((e) => e.payload.value)
	console.log(all)
	
}

// dropLocalOrbitDb()
